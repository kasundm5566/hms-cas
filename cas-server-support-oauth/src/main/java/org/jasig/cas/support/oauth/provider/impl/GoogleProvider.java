package org.jasig.cas.support.oauth.provider.impl;

import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jasig.cas.support.oauth.provider.BaseOAuthProvider;
import org.jasig.cas.support.oauth.provider.UserDetailsCache;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.GoogleApi;
import org.scribe.model.Token;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is the Google provider to authenticate user in Google.
 *
 * @author Jerome Leleu
 */
public class GoogleProvider extends BaseOAuthProvider {

    //HttpSession session = request.getSession(true);

    @Override
    public void initService() {
        service = new ServiceBuilder().provider(GoogleApi.class).apiKey(key).apiSecret(secret)
                .scope("https://www.googleapis.com/auth/userinfo.profile").callback(callbackUrl).build();
    }

    @Override
    public String saveRequestTokenAndGetAuthorizationUrl(HttpSession session) {

        Token requestToken = service.getRequestToken();
        logger.debug("requestToken : " + requestToken);
        // save requestToken in session
        session.setAttribute(name + "#tokenRequest", requestToken);
        String authorizationUrl = "https://www.google.com/accounts/OAuthAuthorizeToken?oauth_token="
                + requestToken.getToken();
        logger.debug("authorizationUrl : " + authorizationUrl);
        logger.debug("session is  : " + session);
        session.setAttribute("social_network_url",authorizationUrl);
        session.setAttribute("google_url",authorizationUrl);
        return authorizationUrl;

    }

    @Override
    protected String getProfileUrl() {
        return "https://www.googleapis.com/oauth2/v1/userinfo?alt=json";
    }

    @Override
    protected Map<String, String> extractUserDetails(String body) {

        Map<String, String> details = new HashMap<String, String>();

        String userId = null;
        try {
            logger.debug("user details " + body);
            JSONObject jsonObject = new JSONObject(body);

            userId = jsonObject.getString("id");

            details.put("id", userId);
            details.put("first_name", (String) jsonObject.get("given_name"));
            details.put("last_name", (String) jsonObject.get("family_name"));

            logger.debug("userId : " + userId);
        } catch (JSONException e) {
            logger.error("JSON exception", e);
            return null;
        }
        return details;
    }
}
