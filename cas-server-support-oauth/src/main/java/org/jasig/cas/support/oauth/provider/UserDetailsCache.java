/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package org.jasig.cas.support.oauth.provider;

import java.util.concurrent.ConcurrentHashMap;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserDetailsCache {

    public static final ConcurrentHashMap<String, String> data = new ConcurrentHashMap<String, String>();

    public static String put(String key, String value) {
        return data.put(key, value);
    }

    public static String get(Object key) {
        return data.get(key);
    }
}
