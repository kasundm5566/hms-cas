package org.jasig.cas.support.oauth.provider.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jasig.cas.support.oauth.provider.BaseOAuthProvider;
import org.jasig.cas.support.oauth.provider.UserDetailsCache;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is the Facebook provider to authenticate user in Facebook.
 * 
 * @author Jerome Leleu
 */
public class FacebookProvider extends BaseOAuthProvider {

  // HttpSession session = request.getSession(true);

    
    @Override
    public void initService() {
        service = new ServiceBuilder().provider(FacebookApi.class).apiKey(key).apiSecret(secret).callback(callbackUrl)
            .build();
    }
    
    @Override
    public String saveRequestTokenAndGetAuthorizationUrl(HttpSession session) {
        // no requestToken for facebook -> no need to save it in session
        String authorizationUrl = service.getAuthorizationUrl(null);
        logger.debug("authorizationUrl : " + authorizationUrl);
        logger.debug("session is  : " + session);
        session.setAttribute("social_network_url",authorizationUrl);
        session.setAttribute("facebook_url",authorizationUrl);
        return authorizationUrl;
    }
    
    @Override
    public Token getAccessToken(HttpSession session, String token, String verifier) {
        logger.debug("verifier : " + verifier);
        Verifier providerVerifier = new Verifier(verifier);
        Token accessToken = service.getAccessToken(null, providerVerifier);
        logger.debug("accessToken : " + accessToken);
        return accessToken;
    }
    
    @Override
    protected String getProfileUrl() {
        return "https://graph.facebook.com/me";
    }
    
    @Override
    protected Map<String, String> extractUserDetails(String body) {

        Map<String, String> details = new HashMap<String, String>();

        String userId = null;
        try {

            logger.debug("User details " +  body);

            JSONObject jsonObject = new JSONObject(body);

            userId = (String) jsonObject.get("id");

            details.put("id", userId);
            details.put("first_name", (String) jsonObject.get("first_name"));
            details.put("last_name", (String) jsonObject.get("last_name"));
            details.put("gender", (String) jsonObject.get("gender"));

            logger.debug("userId : " + userId);

        } catch (JSONException e) {
            logger.error("JSON exception", e);
            return null;
        }
        return details;
    }
    
    @Override
    public String extractTokenFromRequest(HttpServletRequest request) {
        return null;
    }
    
    @Override
    public String extractVerifierFromRequest(HttpServletRequest request) {
        return request.getParameter("code");
    }
}
