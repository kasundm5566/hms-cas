package org.jasig.cas.support.oauth.authentication.principal;

import org.apache.log4j.Logger;
import org.jasig.cas.authentication.principal.AbstractPersonDirectoryCredentialsToPrincipalResolver;
import org.jasig.cas.authentication.principal.Credentials;
import org.jasig.cas.authentication.principal.CredentialsToPrincipalResolver;

import java.util.Map;

/**
 * This class resolves the principal id regarding the OAuth credentials : principal id is the type of the provider # the user identifier.
 * 
 * @author Jerome Leleu
 */
public class OAuthCredentialsToPrincipalResolver extends AbstractPersonDirectoryCredentialsToPrincipalResolver
    implements CredentialsToPrincipalResolver {
    
    private static Logger logger = Logger.getLogger(OAuthCredentialsToPrincipalResolver.class);
    
    @Override
    protected String extractPrincipalId(final Credentials credentials) {
        
        OAuthCredentials oauthCredentials = (OAuthCredentials) credentials;
        
        String uid = oauthCredentials.getProviderName() + "#" + oauthCredentials.getUserId();
        
        logger.debug("extractPrincipalId / uid : " + uid);
        
        return uid;
    }

    @Override
    protected Map<String, Object> extractAttributes(Credentials credentials) {
        OAuthCredentials oAuthCredentials = (OAuthCredentials) credentials;
        return (Map)oAuthCredentials.getAdditionalDetails();
    }

    /**
     * Return true if Credentials are OAuthCredentials, false otherwise.
     */
    public boolean supports(final Credentials credentials) {
        return credentials != null && (OAuthCredentials.class.isAssignableFrom(credentials.getClass()));
    }

}
