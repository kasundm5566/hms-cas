package org.jasig.cas.support.oauth.provider.impl;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.jasig.cas.support.oauth.provider.BaseOAuthProvider;
import org.jasig.cas.support.oauth.provider.UserDetailsCache;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is the Twitter provider to authenticate user in Twitter.
 * 
 * @author Jerome Leleu
 */
public class TwitterProvider extends BaseOAuthProvider {

    //HttpSession session = request.getSession(true);

    @Override
    public void initService() {
        service = new ServiceBuilder().provider(TwitterApi.class).apiKey(key).apiSecret(secret).callback(callbackUrl)
            .build();
    }
    
    @Override
    public String saveRequestTokenAndGetAuthorizationUrl(HttpSession session) {
        Token requestToken = service.getRequestToken();
        logger.debug("requestToken : " + requestToken);
        // save requestToken in session
        session.setAttribute(name + "#tokenRequest", requestToken);
        String authorizationUrl = service.getAuthorizationUrl(requestToken);
        logger.debug("authorizationUrl : " + authorizationUrl);
        logger.debug("session is  : " + session);
        session.setAttribute("social_network_url",authorizationUrl);
        session.setAttribute("twitter_url",authorizationUrl);
        return authorizationUrl;
    }
    
    @Override
    protected String getProfileUrl() {
        return "http://api.twitter.com/1/account/verify_credentials.xml";
    }
    
    @Override
    protected Map<String, String> extractUserDetails(String body) {

        Map<String, String> details = new HashMap<String, String>();

        String userId = StringUtils.substringAfter(StringUtils.substringBefore(body, "</id>"), "<id>");

        String name = StringUtils.substringAfter(StringUtils.substringBefore(body, "</name>"), "<name>");

        String firstName = "";
        String lastName = "";

        if (name.indexOf(' ') != -1) {
            firstName = name.substring(0, name.indexOf(' ')).trim();
            lastName = name.substring(name.indexOf(' ')).trim();
        } else {
            firstName = name;
        }

        logger.debug("userId : " + userId + ", name : " + name);

        details.put("id", userId);
        details.put("first_name", firstName);
        details.put("last_name", lastName);

        return details;
    }
}
