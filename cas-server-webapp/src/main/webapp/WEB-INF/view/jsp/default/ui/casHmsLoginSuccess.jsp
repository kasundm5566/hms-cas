<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="org.jasig.cas.support.oauth.authentication.principal.OAuthCredentials"%>
<%@ page import="org.jasig.cas.authentication.principal.UsernamePasswordCredentials" %>
<%
    String userId = "";
    String provider = "";
    String service = request.getAttribute("service").toString();
    String ticket = (String) request.getAttribute("serviceTicketId");
    boolean redirect = false;

    Object credentials = request.getAttribute("credentials");

    if (credentials == null) {
        out.println("Unknown credential type [null]");
    } else if (credentials instanceof OAuthCredentials) {
        userId = ((OAuthCredentials) credentials).getUserId();
        provider = ((OAuthCredentials) credentials).getProviderName();
        redirect = true;
    } else if (credentials instanceof UsernamePasswordCredentials) {
        userId = ((UsernamePasswordCredentials) credentials).getUsername();
        provider = "internal";
        redirect = true;
    } else {
        out.println("Unknown credential type [" + credentials.getClass() + "]");
    }

    if (redirect) {
        response.sendRedirect(service + "?" + "ticket=" + ticket);
    }
 //response.sendRedirect("http://59.163.254.24:4287/registration/additional/#common/openid?service=test");
 //response.sendRedirect("http://59.163.254.24:4287/registration/additional/#common/openid?service=test");
%>
