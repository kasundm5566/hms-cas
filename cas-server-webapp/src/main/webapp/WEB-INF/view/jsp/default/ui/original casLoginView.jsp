<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<jsp:directive.include file="includes/top.jsp" />
<form:form method="post" id="fm1" cssClass="fm-v clearfix" commandName="${commandName}" htmlEscape="true">
    <form:errors path="*" cssClass="errors" id="status" element="div" />
    <div class="center-layout">
        <div class="login-box">
            <h3><spring:message code="login.panel.heading"/></h3>

            <div class="box">
                <div class="box-upper">
                <div class="row">
                    <label for="username"><spring:message code="screen.welcome.label.netid" /></label>
                    <c:if test="${not empty sessionScope.openIdLocalId}">
                        <strong>${sessionScope.openIdLocalId}</strong>
                        <input type="hidden" id="username" name="username" value="${sessionScope.openIdLocalId}" />
                    </c:if>

                    <c:if test="${empty sessionScope.openIdLocalId}">
                        <spring:message code="screen.welcome.label.netid.accesskey" var="userNameAccessKey" />
                        <form:input cssClass="required" cssErrorClass="error" id="username" size="32" tabindex="1" accesskey="${userNameAccessKey}" path="username" autocomplete="false" htmlEscape="true" />
                    </c:if>
                </div>
                <div class="row">
                    <label for="password"><spring:message code="screen.welcome.label.password" /></label>
                    <spring:message code="screen.welcome.label.password.accesskey" var="passwordAccessKey" />
                    <form:password cssClass="required" cssErrorClass="error" id="password" size="32" tabindex="2" path="password"  accesskey="${passwordAccessKey}" htmlEscape="true" autocomplete="off" />
                </div>

                <input type="hidden" name="lt" value="${flowExecutionKey}" />
                <input type="hidden" name="_eventId" value="submit" />
                <div>
                    <input class="action-button" name="submit" accesskey="l" value="<spring:message code="screen.welcome.button.login" />" tabindex="4" type="submit" />
                </div>
                </div>
                <div class="horizontal-line"></div>
                <div class="align-bottom">
                    <div class="individual-acc-link">
                        <a href="http://cur.vck:4287/registration/default/#common/account/recovery"><spring:message code="registration.account.recovery.link"/></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="sign-in-box">
            <h3><spring:message code="sign.panel.heading"/></h3>
            <div class="box">
                <div class="box-upper">
                    <div class="openid-link-layout">
                        <a href="${facebook1_authorizationUrl}" target="_blank"><img src="images/fb-icon.jpg" class="sign-in-icon"></a>
                        <a href="${twitter1_authorizationUrl}" target="_blank"><img src="images/twitter-icon.jpg" class="sign-in-icon"></a>
                        <a href="${google1_authorizationUrl}" target="_blank"><img src="images/google-icon.jpg" class="sign-in-icon"></a>
                    </div>
                    <div class="new-account-label">
                        <label for="individualUserCreation" class="individual-user-label"><spring:message code="registration.no.user.id.msg"/></label>

                        <a id="individual_usr_link_temp" href="http://cur.vck:4287/registration/default/#common/individual"><spring:message code="registration.create.individual.user.link" /></a>
                    </div>
                    <div class="panel-clear-space"></div>
                </div>
                <div class="horizontal-line"></div>
                <div class="corporate-acc-link">

                    <a href="http://cur.vck:4287/registration/default/#common/corporate"><spring:message code="registration.create.corporate.user.link"/></a>
                    <spring:message code="registration.only.for.application.developers.link"/>

                    <div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

</form:form>
</div>
<jsp:directive.include file="includes/bottom.jsp" />



