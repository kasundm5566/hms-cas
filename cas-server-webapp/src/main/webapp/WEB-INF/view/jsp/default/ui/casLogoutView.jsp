<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
<div class="main-layout">
    <jsp:directive.include file="includes/top.jsp" />
</div>
<div class="center-layout">
    <div id="msg" class="success">
        <h2><spring:message code="screen.logout.header" /></h2>

        <p><spring:message code="screen.logout.success" /></p>
        <p><spring:message code="screen.logout.security" /></p>
        <br>
        <p><b>Login to</b></p>

        <%--
         Implementation of support for the "url" parameter to logout as recommended in CAS spec section 2.3.1.
         A service sending a user to CAS for logout can specify this parameter to suggest that we offer
         the user a particular link out from the logout UI once logout is completed.  We do that here.
        --%>
<%--
        <c:if test="${not empty param['url']}">
            <p>
                <spring:message code="screen.logout.redirect" arguments="${fn:escapeXml(param.url)}" />
            </p>
        </c:if>
        <div style="padding-left:8px;">
            <b><a href="/registration/"> Registration </a></b><br>
            <b><a href="/provisioning/c/portal/login"> Provisioning </a></b><br>
            <b><a href="/soltura"> Soltura </a></b><br>
            <b><a href="/appstore"> Appstore </a></b><br>
            <b><a href="/appstore"> SDP Admin </a></b><br>
            <b><a href="/appstore"> Reporting </a></b><br>
            <b><a href="/appstore"> PGW Self Care </a></b><br>
            <b><a href="/appstore"> PGW Admin Care </a></b><br>

        </div>
    </div>
</div>
</div>
<jsp:directive.include file="includes/bottom.jsp" />
</div>--%>

<%
    String serviceUrl = null;
    boolean noError = false;
    String redirectUrl = null;

    try {

        serviceUrl = (String) request.getParameter("service");
    } catch (NullPointerException ex) {
        serviceUrl = null;
    }
    try {
        redirectUrl = (String) request.getParameter("redirectUrl");
    } catch (NullPointerException ex) {
        redirectUrl = null;
    }
    try {
        noError = Boolean.parseBoolean(request.getParameter("noerror"));
    } catch (NullPointerException ex) {
        noError = false;
    }

    if(redirectUrl != null){
        response.sendRedirect(redirectUrl);
    } else if (serviceUrl != null) {
        response.sendRedirect("/cas/login?success=cas.logout.successful&service=" + serviceUrl);
    } else if (noError) {
        response.sendRedirect("/cas/login");
    } else {
        response.sendRedirect("/cas/login?success=cas.logout.successful");
    }


%>