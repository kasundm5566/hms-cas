<jsp:directive.include file="includes/mobile_top.jsp" />
<form:form method="post" id="fm1" cssClass="fm-v clearfix" commandName="${commandName}" htmlEscape="true">
    <form:errors path="*" cssClass="errors" id="status" element="div" />

          <%
          //start

          //error
             String error=null;

             error=request.getParameter("error");


              if(error!=null){
             %>

             <div class="errors" id="status" >

               <spring:message code="<%=error%>"/>


             </div>

           <%
               }  //error ends
           %>

           <%
            //success

          String success=null;

          success=request.getParameter("success");


           if(success!=null){
          %>

          <div class="success" id="status" >

            <spring:message code="<%=success%>"/>


          </div>

        <%
            } //success ends
            //end
        %>

    <c:if test="${custom_error} neq null">
        <div id="status" class="errors">
            ${custom_error}
        </div>
    </c:if>

<div id="breadcrumb">
    <h2 class="breadcrumb-left"><spring:message code="login.panel.heading"/></h2>
    <br class="clear-all" />
</div><!-- end of div#breadcrumb -->

<div id="main-content">
    <div id="login-container">

       	  <fieldset class="username">
        	<label for="username"><spring:message code="screen.welcome.label.netid" /></label>

                    <c:if test="${empty sessionScope.openIdLocalId}">
                        <spring:message code="screen.welcome.label.netid.accesskey" var="userNameAccessKey" />
                        <form:input cssClass="required" cssErrorClass="error" id="username" size="32" tabindex="1" accesskey="${userNameAccessKey}" path="username" autocomplete="false" htmlEscape="true" />
                    </c:if>
          </fieldset>

            <fieldset class="password">
                    <label for="password"><spring:message code="screen.welcome.label.password" /></label>
                    <spring:message code="screen.welcome.label.password.accesskey" var="passwordAccessKey" />
                    <form:password cssClass="required" cssErrorClass="error" id="password" size="32" tabindex="2" path="password"  accesskey="${passwordAccessKey}" htmlEscape="true" autocomplete="off" />
          </fieldset>

            <fieldset class="login">
            <input class="action-button" name="submit" accesskey="l" value="<spring:message code="screen.welcome.button.login" />" tabindex="4" type="submit" />
          </fieldset>
            <fieldset class="keep-me-signed-in">
          </fieldset>

                <input type="hidden" name="lt" value="${flowExecutionKey}" />
                <input type="hidden" name="_eventId" value="submit" />
<!--
<div class="new-account-phone-verification"><a href="https://vdf.ctap.hsenidmobile.com/registration/default#common/account/recovery"><spring:message code="registration.account.recovery.link"/></a></div>
<div class="signin-with">  Sign In With  </div>
                    <div class="icons">

                        <a href="/cas/facebook"><img src="images/social_facebook_box_blue_256.png" class="sign-in-icon"></a>
                        <a href="/cas/twitter"><img src="images/social_twitter_box_blue_256.png" class="sign-in-icon"></a>
          <a href="/cas/google"><img  src="images/social_google_box_256.png" class="sign-in-icon"></a>
                    </div>

      <div class="social-icons"><spring:message code="registration.no.user.id.msg"/><span class="dont-have-account"><a id="individual_usr_link_temp" href="https://vdf.ctap.hsenidmobile.com/registration/default#common/individual"><spring:message code="registration.create.individual.user.link" /></a></span></div>

          -->
        <!--ends-->


    </div><!-- end of div#login-container -->
</div><!-- end of div#main-content -->

</form:form>
<div id="footer">
    <p> 2014, hSenid Mobile (pvt) Ltd.</p>
</div><!-- end of div#footer -->
</body></html>

