<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:directive.include file="includes/top.jsp"/>

<script type="text/javascript">
    setTimeout("location.reload()", 1000 * 240);
</script>

<form:form method="post" id="fm1" cssClass="fm-v clearfix" commandName="${commandName}" htmlEscape="true">


    <!--custom-->

    <%

        String error = null;

        error = request.getParameter("error");


        if (error != null) {
    %>

    <div class="errors" id="status2error">

        <h3 class="messageBox"><spring:message code="<%=error%>"/></h3>
    </div>

    <%
        }
    %>


    <!--custom error-->


    <%

        String success = null;

        success = request.getParameter("success");


        if (success != null) {
    %>
    <div class="success" id="status2success">

        <h3 class="messageBox"><spring:message code="<%=success%>"/></h3>
    </div>
    <%
        }
    %>


    <div style="margin: 0 auto; width: 65%;">
    <form:errors path="*" cssClass="errors" id="status" element="div">
    </form:errors>
        </div>

    <script type="text/javascript">
        var msg = document.getElementById("status").innerHTML;
        if (msg.length != 0) {
            document.getElementById('status2success').parentNode.removeChild(document.getElementById('status2success'))
        }
    </script>


    <!--custom success-->
    <c:if test="${custom_error} neq null">
        <div id="status" class="errors">
            ${custom_error}
        </div>
    </c:if>

    <div class="center-layout">
        <div class="left_col">
        <div class="login-box">
            <h3><spring:message code="login.panel.heading"/></h3>

            <div class="box">
                <div class="box-upper">
                    <div class="row">
                        <spring:message code="screen.welcome.label.netid"/>
                        <label for="username"></label><br/>
                        <c:if test="${not empty sessionScope.openIdLocalId}">
                            <strong>${sessionScope.openIdLocalId}</strong>
                            <input type="hidden" id="username" name="username" value="${sessionScope.openIdLocalId}"/>
                        </c:if>

                        <c:if test="${empty sessionScope.openIdLocalId}">
                            <spring:message code="screen.welcome.label.netid.accesskey" var="userNameAccessKey"/>
                            <form:input cssClass="required loginInputSize" cssErrorClass="error" id="username" tabindex="1"
                                        accesskey="${userNameAccessKey}" path="username" autocomplete="false"
                                        htmlEscape="true"/>
                        </c:if>
                    </div>
                    <div class="row">
                        <spring:message code="screen.welcome.label.password"/>
                        <label for="password"></label><br/>
                        <spring:message code="screen.welcome.label.password.accesskey" var="passwordAccessKey"/>
                        <form:password cssClass="required loginInputSize" cssErrorClass="error" id="password" tabindex="2"
                                       path="password" accesskey="${passwordAccessKey}" htmlEscape="true"
                                       autocomplete="off"/>
                    </div>

                    <input type="hidden" name="lt" value="${flowExecutionKey}"/>
                    <input type="hidden" name="_eventId" value="submit"/>

                    <div>
                        <input class="action-button" name="submit" accesskey="l" value="<spring:message code="screen.welcome.button.login"/>" tabindex="4" type="submit" />
                    </div>
                </div>
                <div class="horizontal-line"></div>
                <div class="align-bottom">
                    <div class="individual-acc-link">
                        <a href="https://dev.sdp.hsenidmobile.com/registration/default#common/account/recovery"><spring:message code="registration.account.recovery.link"/></a>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="right_col">

            <div class="panel_header">Register for a new Account</div>

            <div id="account-links">
                    <p><spring:message code="registration.create.individual.user.link"/></p>
                    <a href="https://dev.sdp.hsenidmobile.com/registration/default#common/individual" class="register_button">
                        <spring:message code="registration.create.user.link"/>
                    </a>
                    <br/><br/>

                    <p><spring:message code="registration.create.corporate.user.link"/>
                        <spring:message code="registration.only.for.application.developers.link"/>
                    </p>
                <a href="https://dev.sdp.hsenidmobile.com/registration/default#common/corporate" class="register_button">
                    <spring:message code="registration.create.user.link"/>
                </a>
            </div>
            </div>
        </div>

    </div>
    </div>

</form:form>
</div>
<br/>
<jsp:directive.include file="includes/bottom.jsp"/>
