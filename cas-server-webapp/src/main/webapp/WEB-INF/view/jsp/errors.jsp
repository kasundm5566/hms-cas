<jsp:directive.include file="default/ui/includes/top.jsp" />
		<div id="welcome">
			<h2>CAS is Unavailable</h2>

			<p>
			   There was an error trying to complete your request.  Please notify your support desk or try again.
                <% response.sendRedirect("/cas/login");%>
			</p>
		</div>
<jsp:directive.include file="default/ui/includes/bottom.jsp" />
