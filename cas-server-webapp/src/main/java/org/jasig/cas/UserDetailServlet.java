/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package org.jasig.cas;

import org.jasig.cas.support.oauth.provider.UserDetailsCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserDetailServlet extends HttpServlet{

    Logger logger = LoggerFactory.getLogger(UserDetailServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String provider = req.getParameter("provider");
        String userId = req.getParameter("user_id");
        String key = userId + "@" + provider;
        logger.debug("Finding user details for [{}]", key);
        String response = UserDetailsCache.get(key);
        logger.debug("Found user details for [{}] is [{}]", key, response);
        resp.setContentType("application/json");
        resp.getWriter().append(response == null ? "{}" : response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
