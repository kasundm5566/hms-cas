package org.jasig.cas.social;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import org.jasig.cas.support.oauth.provider.impl.GoogleProvider;
import org.jasig.cas.support.oauth.provider.impl.FacebookProvider;
import org.jasig.cas.support.oauth.provider.impl.TwitterProvider;

/**

 */
public class SocialNetworkServlet extends HttpServlet {

    ApplicationContext appContext;


    protected static Logger logger = Logger.getLogger(SocialNetworkServlet.class);

    public void init(ServletConfig servletConfig) throws ServletException {

        appContext = WebApplicationContextUtils.getWebApplicationContext(servletConfig.getServletContext());

    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        //String authorizationUrl = "https://vdf.ctap.hsenidmobile.com/cas/login";
        String authorizationUrl = "/cas/login";
        String service;

        GoogleProvider google = (GoogleProvider) appContext.getBean("google");
        FacebookProvider facebook = (FacebookProvider) appContext.getBean("facebook");
        TwitterProvider twitter = (TwitterProvider) appContext.getBean("twitter");


        //getting the service URL

        try {
            service = session.getAttribute("service").toString();
            //this authorization url will be used to redirect  to the cas login when there is a
            authorizationUrl = authorizationUrl + "?service=" + service;

            logger.debug(" service url is found and service url is [" + service + "]");
        } catch (NullPointerException ex) {

            service = null;
            logger.debug("service url is not found");
        }


        out.println(" Application context is  " + appContext);

        String requestURI = request.getRequestURI().toString();

        String requestURL = request.getRequestURL().toString();


        logger.debug(" request URI is " + requestURI);

        logger.debug(" request URL is " + requestURL);



        try {
            if (requestURI.equalsIgnoreCase("/cas/facebook")) {

                authorizationUrl = facebook.saveRequestTokenAndGetAuthorizationUrl(session);

            } else if (requestURI.equalsIgnoreCase("/cas/twitter")) {

                authorizationUrl = twitter.saveRequestTokenAndGetAuthorizationUrl(session);

            } else if (requestURI.equalsIgnoreCase("/cas/google")) {

                authorizationUrl = google.saveRequestTokenAndGetAuthorizationUrl(session);
            }

            logger.debug("Authorization url is " + authorizationUrl);

            response.sendRedirect(authorizationUrl);

        } catch (Exception ex) {

            logger.debug("Exception was generated while generating authorization url for google  " , ex);

            response.sendRedirect(authorizationUrl);

        } //catch


    } //doGet

} //Servlet class
