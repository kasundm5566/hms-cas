package org.jasig.cas.web.flow;

import org.jasig.cas.web.support.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.action.AbstractAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 */
public class CheckUserAgent extends AbstractAction {

    private static final Logger logger = LoggerFactory.getLogger(CheckUserAgent.class);

    private Map<Pattern,String> mobileTypes;
   // private Map mobileTypes;


    @Override
    protected Event doExecute(RequestContext requestContext) throws Exception {

        final HttpServletRequest request = WebUtils.getHttpServletRequest(requestContext);

        String userAgent = request.getHeader("User-Agent");
        String accept = request.getHeader("accept");
        String wapProfile = request.getHeader("x-wap-profile");

        logger.debug(" actual user agent found in web flow ************* "+userAgent);

        // userAgent="Mozilla/5.0 (SymbianOS/9.4; Series60/5.0 Nokia5800d-1/50.0.005; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Version/3.0 BrowserNG/7.2.3";

        if(isMobileBrowser(userAgent, accept, wapProfile)){

            logger.debug("**************mobile browser found**************");
            requestContext.getFlowScope().put("mobile",true);

        }
        else{

            logger.debug("*********HTML Web browser *******************");
            requestContext.getFlowScope().put("mobile",false);

        }

        return result("success");

    }//doExecute



    public boolean isMobileBrowser(String userAgent, String accept, String wapProfile){

        logger.debug(" Detecting the browser type [ whether mobile or html]");

        /*
        Map<Pattern,String> mobileTypes=new HashMap<Pattern, String>();

        mobileTypes.put(Pattern.compile(".*iPhone.*"),"iphone");
        mobileTypes.put(Pattern.compile(".*Android.*"),"android");
        mobileTypes.put(Pattern.compile(".*Safari.*Pre.*"),"safari");
        mobileTypes.put(Pattern.compile(".*Nokia.*AppleWebKit.*"),"nokia");
        mobileTypes.put(Pattern.compile(".*SymbianOS*"),"nokia");
        mobileTypes.put(Pattern.compile(".*SymbOS*"),"nokia");
         */


        for (final Map.Entry<Pattern,String> entry : mobileTypes.entrySet()) {

         logger.debug("******Pattern is ["+entry.getKey()+"] and key is["+entry.getValue()+"]**********");

            if (entry.getKey().matcher(userAgent).matches()) {

                logger.debug(" request is from a mobile browser ");
                return true;

            }
        }  //for

        if (wapProfile != null) {
            return true;
        }

        if (accept != null && (accept.contains("wap") || accept.contains("wml"))) {
            return true;
        }

        logger.debug(" request is from pc browser (not mobile browser) ");
        return false;

    }//method


    public void setMobileTypes(Map<String,String> mobileAgentTypes) {

        this.mobileTypes = new HashMap<Pattern, String>();

        logger.debug("*********inside setter method*********");
        for (final Map.Entry<String,String> entry : mobileAgentTypes.entrySet()) {

            this.mobileTypes.put(Pattern.compile(entry.getKey()), entry.getValue());
            logger.debug("******Pattern is ["+entry.getKey()+"] and key is["+entry.getValue()+"]**********");
        }

    }//setter

}//class
