/*
 * Copyright 2007 The JA-SIG Collaborative. All rights reserved. See license
 * distributed with this file and available online at
 * http://www.uportal.org/license.html
 */
package org.jasig.cas.authentication.principal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.org.apache.bcel.internal.generic.NEW;
import org.jasig.services.persondir.IPersonAttributeDao;
import org.jasig.services.persondir.IPersonAttributes;
import org.jasig.services.persondir.support.StubPersonAttributeDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;

/**
 * 
 * @author Scott Battaglia
 * @version $Revision$ $Date$
 * @since 3.1
 *
 */
public abstract class AbstractPersonDirectoryCredentialsToPrincipalResolver
    implements CredentialsToPrincipalResolver {

    /** Log instance. */
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    private boolean returnNullIfNoAttributes = false;
    
    /** Repository of principal attributes to be retrieved */
    @NotNull
    private IPersonAttributeDao attributeRepository = new StubPersonAttributeDao(new HashMap<String, List<Object>>());

    public final Principal resolvePrincipal(final Credentials credentials) {
        if (log.isDebugEnabled()) {
            log.debug("Attempting to resolve a principal...");
        }

        final String principalId = extractPrincipalId(credentials);
        
        if (principalId == null) {
            return null;
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Creating SimplePrincipal for ["
                + principalId + "]");
        }

        final IPersonAttributes personAttributes = this.attributeRepository.getPerson(principalId);

        Map<String, List<Object>> attributes;

        if (personAttributes == null) {
            attributes = null;
        } else {
            attributes = personAttributes.getAttributes();
        }

        if (attributes == null & !this.returnNullIfNoAttributes) {
            return new SimplePrincipal(principalId);
        }

        if (attributes == null) {
            Map<String, Object> directAttributes = extractAttributes(credentials);
            log.debug("attributes are null, direct attributes are " + directAttributes);
            if (directAttributes == null) {
                log.debug("return null.......");
                return null;
            } else {
                log.debug("return simple principle with direct attributes");
                new SimplePrincipal(principalId, directAttributes);
            }
            return null;
        } else {
            log.debug("Add all credential attributes");
            Map<String, List<Object>> customAttributes = attributes;

            attributes = new HashMap<String, List<Object>>();

            attributes.putAll(customAttributes);
            attributes.putAll(doExtractAttributes(credentials));
            log.debug("Resulted attributes are " + attributes);
        }


        final Map<String, Object> convertedAttributes = new HashMap<String, Object>();
        
        for (final Map.Entry<String, List<Object>> entry : attributes.entrySet()) {
            final String key = entry.getKey();
            final Object value = entry.getValue().size() == 1 ? entry.getValue().get(0) : entry.getValue();
            convertedAttributes.put(key, value);
        }
        if(log.isDebugEnabled()) {
            log.debug("create principle with  converted attributes : " + convertedAttributes);
        }
        return new SimplePrincipal(principalId, convertedAttributes);
    }

    private Map<String, List<Object>> doExtractAttributes(Credentials credentials) {
        Map<String, Object> attributes = extractAttributes(credentials);
        Map<String, List<Object>> convertedAttributes = new HashMap<String, List<Object>>();

        for (Map.Entry<String, Object> entry : attributes.entrySet()) {
            ArrayList<Object> value = new ArrayList<Object>();
            value.add(entry.getValue());
            convertedAttributes.put(entry.getKey(), value);
        }
        return convertedAttributes;
    }

    protected Map<String, Object> extractAttributes(Credentials credentials) {
        HashMap<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("first_name", "test");
        attributes.put("last_name", "test");
        attributes.put("gender", "test");
        return attributes;
    }
    
    /**
     * Extracts the id of the user from the provided credentials.
     * 
     * @param credentials the credentials provided by the user.
     * @return the username, or null if it could not be resolved.
     */
    protected abstract String extractPrincipalId(Credentials credentials);
    
    public final void setAttributeRepository(final IPersonAttributeDao attributeRepository) {
        this.attributeRepository = attributeRepository;
    }

    public void setReturnNullIfNoAttributes(final boolean returnNullIfNoAttributes) {
        this.returnNullIfNoAttributes = returnNullIfNoAttributes;
    }
}
