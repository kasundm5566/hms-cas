# Central Authentication Service - Build and Deploy Instructions

Software Dependencies
=====================

java      - 1.7
maven     - 3.0.3


Build CAS
=====================

1. Build project using maven

   mvn clean install or mvn clean install -DskipTests
   
   mvn clean install -o or mvn clean install -DskipTests -o (offline mode)

2. Deploy the build files to tomcat server

   [cas-server-webapp/target/cas.war]
